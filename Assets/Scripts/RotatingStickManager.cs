﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotatingStickManager : MonoBehaviour
{
    [SerializeField] private int direction = 1;

    void Start()
    {
        transform.DOLocalRotate(new Vector3(0, 360 * direction, 0), 5f, RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
    }
}
