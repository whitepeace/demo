﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HorizontalObstacleManager : MonoBehaviour
{
    private Vector3 startPosition;
    [SerializeField] private Vector3 destination;
    [SerializeField] private float requiredTime = 1f;

    public int CurrentDirection = 0; // -1: to left, 1: to right

    void Start()
    {
        startPosition = transform.position;
        StartCoroutine(MoveEnum());
    }

    private IEnumerator MoveEnum()
    {
        if (destination.x > transform.position.x) CurrentDirection = 1;
        else CurrentDirection = -1;

        while(true)
        {
            transform.DOLocalMove(destination, requiredTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(requiredTime);
            CurrentDirection *= -1;
            transform.DOLocalMove(startPosition, requiredTime).SetEase(Ease.Linear);
            yield return new WaitForSeconds(requiredTime);
            CurrentDirection *= -1;
        }
    }
}
