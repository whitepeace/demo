﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotatingPlatformManager : MonoBehaviour
{
    [SerializeField] private int direction = 1;

    void Start()
    {
        transform.DOLocalRotate(new Vector3(0, 0, 360 * direction), 20f, RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
    }
}
