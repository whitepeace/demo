﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentManager : MonoBehaviour
{
    [SerializeField] private float speed = 10f;

    private Rigidbody rb;
    private Animator anim;
    private Vector3 rbVelocity;

    private Vector3 startPosition;
    public bool isUnderForce = false;

    [SerializeField] private List<Obstacle> nearbyObstacles;

    private int checkPointIndex = -1;

    [SerializeField] private bool finished = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.SetTrigger("Idle");
        startPosition = transform.position;
    }

    void Update()
    {
        if (finished || !GameManager.instance.IsGameStarted) return;

        rbVelocity = transform.forward * speed;

        float closestDistance = Mathf.Infinity;
        int closestObstacleIndex = -1;
        for(int i=0; i < nearbyObstacles.Count; i++)
        {
            float obstacleDistance = Vector3.Distance(transform.position, nearbyObstacles[i].transform.position);
            if(obstacleDistance < closestDistance)
            {
                closestDistance = obstacleDistance;
                closestObstacleIndex = i;
            }
        }

        if(closestObstacleIndex != -1)
        {
            if (closestDistance < 10f)
            {
                if (nearbyObstacles[closestObstacleIndex].ObstacleType == 0)
                {
                    if (nearbyObstacles[closestObstacleIndex].transform.position.x > transform.position.x)
                    {
                        rbVelocity += Vector3.left * speed * 0.4f;
                    }
                    else
                    {
                        rbVelocity += Vector3.right * speed * 0.4f;
                    }
                }
                else if (nearbyObstacles[closestObstacleIndex].ObstacleType == 1)
                {
                    HorizontalObstacleManager horizontalObstacle = nearbyObstacles[closestObstacleIndex].GetComponent<HorizontalObstacleManager>();

                    if (Mathf.Abs(nearbyObstacles[closestObstacleIndex].transform.position.x - transform.position.x) < 4f)
                    {

                        if (horizontalObstacle != null)
                        {
                            if (transform.position.x - horizontalObstacle.transform.position.x < 0 && horizontalObstacle.CurrentDirection == 1 ||
                               transform.position.x - horizontalObstacle.transform.position.x > 0 && horizontalObstacle.CurrentDirection == -1)
                            {
                                rbVelocity += Vector3.left * speed * 0.4f * horizontalObstacle.CurrentDirection;
                            }
                            else
                            {
                                rbVelocity += Vector3.right * speed * 0.4f * horizontalObstacle.CurrentDirection;
                            }
                        }
                        else
                        {
                            if (nearbyObstacles[closestObstacleIndex].transform.position.x > transform.position.x)
                            {
                                rbVelocity += Vector3.left * speed * 0.4f;
                            }
                            else
                            {
                                rbVelocity += Vector3.right * speed * 0.4f;
                            }
                        }
                    }
                }
                else if (nearbyObstacles[closestObstacleIndex].ObstacleType == 2)
                {
                    if (nearbyObstacles[closestObstacleIndex].transform.parent.parent.localEulerAngles.y == 180f)
                    {
                        rbVelocity += Vector3.right * speed * 0.4f;
                    }
                    else if (nearbyObstacles[closestObstacleIndex].transform.parent.parent.localEulerAngles.y == 0f)
                    {
                        rbVelocity += Vector3.left * speed * 0.4f;
                    }
                }
            }

            if (nearbyObstacles[closestObstacleIndex].ObstacleType == 3)
            {
                if(transform.position.z < nearbyObstacles[closestObstacleIndex].transform.position.z + nearbyObstacles[closestObstacleIndex].transform.localScale.z * 0.5f)

                if (nearbyObstacles[closestObstacleIndex].transform.position.x - transform.position.x > 0.2f)
                {
                    rbVelocity += Vector3.right * speed * 0.5f;
                }
                else if (transform.position.x - nearbyObstacles[closestObstacleIndex].transform.position.x > 0.2f)
                {
                    rbVelocity += Vector3.left * speed * 0.5f;
                }
            }
        }


        transform.eulerAngles = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (isUnderForce || finished) return;

        if (transform.position.x < -6 && rbVelocity.x < 0) rbVelocity.x = 0;
        else if (transform.position.x > 6 && rbVelocity.x > 0) rbVelocity.x = 0;
        
        rb.velocity = rbVelocity;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, 25f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Obstacle"))
        {
            nearbyObstacles.Clear();
            ContinueFromNearestCheckPoint();
        }
        else if (collision.collider.CompareTag("RotatingStick"))
        {
            StartCoroutine(ApplyRotatingStickForceEnum(collision.GetContact(0).point));
        }
        else if (collision.collider.CompareTag("RotatingPlatform"))
        {
            transform.SetParent(collision.collider.transform);
        }
        else if(collision.collider.CompareTag("Opponent"))
        {
            Vector3 forceDirection = collision.collider.transform.position - transform.position;
            forceDirection.y = 0;
            forceDirection.Normalize();
            ApplyForce();
            collision.collider.GetComponent<Rigidbody>().AddForce(forceDirection * 1000f);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("RotatingPlatform"))
        {
            transform.SetParent(null);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Obstacle>() != null)
        //if(other.CompareTag("Obstacle") || other.CompareTag("RotatingStick") || other.CompareTag("RotatingPlatform"))
        {
            nearbyObstacles.Add(other.transform.GetComponent<Obstacle>());
        }
        else if(other.CompareTag("CheckPoint"))
        {
            if(other.transform.GetSiblingIndex() > checkPointIndex)
            {
                checkPointIndex++;
            }
        }
        else if(other.CompareTag("AIFinish"))
        {
            finished = true;
            rb.velocity = Vector3.zero;
            anim.SetTrigger("Idle");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Obstacle>() != null)
        //if (other.CompareTag("Obstacle") || other.CompareTag("RotatingStick") || other.CompareTag("RotatingPlatform"))
        {
            nearbyObstacles.Remove(other.transform.GetComponent<Obstacle>());
        }
    }

    private IEnumerator ApplyRotatingStickForceEnum(Vector3 halfDonutPosition)
    {
        Vector3 force = transform.position - halfDonutPosition;
        force.y = 0f;
        force.Normalize();
        rb.velocity = Vector3.zero;
        rb.AddForce(force * 4000f);

        isUnderForce = true;
        yield return new WaitForSeconds(0.5f);
        isUnderForce = false;
    }

    private void ContinueFromNearestCheckPoint()
    {
        if (finished) return;

        if(checkPointIndex == -1)
        {
            transform.position = startPosition;
        }
        else
        {
            Vector3 checkPoint = GameManager.instance.CheckPoints[checkPointIndex].position;
            transform.position = new Vector3(startPosition.x, startPosition.y, checkPoint.z);
        }
    }

    private IEnumerator applyForceEnum;
    public void ApplyForce()
    {
        if(applyForceEnum != null)
        {
            StopCoroutine(applyForceEnum);
        }
        applyForceEnum = ApplyForceEnum();
        StartCoroutine(applyForceEnum);
    }

    private IEnumerator ApplyForceEnum()
    {
        isUnderForce = true;
        yield return new WaitForSeconds(0.3f);
        isUnderForce = false;
    }
}
