﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private RenderTexture rt;

    private Vector3 startPosition;

    private Vector3 previousMousePosition;
    private Vector3 currentMousePosition;
    private Vector3 deltaMousePosition;

    private Rigidbody rb;

    private bool isUnderForce = false;

    private Vector3 rbVelocity;
    private Animator anim;

    private int checkPointIndex = -1;

    void Start()
    {
        SkinnedMeshRenderer smr = transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
        Material[] temp = smr.materials;
        temp[0] = Resources.Load<Material>("Assets/Materials/Boy/clothes1");
        temp[1] = Resources.Load<Material>("Assets/Materials/Boy/skin1");
        //smr.materials = temp;
        smr.materials[0] = Resources.Load<Material>("Assets/Materials/Boy/clothes1");
        smr.materials[1] = Resources.Load<Material>("Assets/Materials/Boy/skin1");

        smr = transform.GetChild(1).GetComponent<SkinnedMeshRenderer>();
        temp = smr.materials;
        temp[0] = Resources.Load<Material>("Assets/Materials/Boy/phong1");
        //smr.materials = temp;
        smr.materials[0] = Resources.Load<Material>("Assets/Materials/Boy/phong1");

        startPosition = transform.position;
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {      
        if (isUnderForce) return;

        if(Input.GetMouseButton(0))
        {
            if(!GameManager.instance.IsGameEnded)
            {
                currentMousePosition = Input.mousePosition;
                deltaMousePosition = currentMousePosition - previousMousePosition;
                if (previousMousePosition != Vector3.zero && deltaMousePosition != Vector3.zero)
                {
                    deltaMousePosition.y = 0f;
                    deltaMousePosition.z = 0f;
                    //rb.velocity = Vector3.forward * speed + deltaMousePosition;

                    deltaMousePosition = Vector3.ClampMagnitude(deltaMousePosition, 10f);
                    
                    rbVelocity = Vector3.forward * speed + deltaMousePosition;
                }
                else
                {
                    rbVelocity = Vector3.forward * speed;
                }
                previousMousePosition = currentMousePosition;

                if (Input.GetMouseButtonDown(0))
                {
                    anim.SetTrigger("Run");
                    GameManager.instance.StartGame();
                }
            }
            else
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if(Physics.Raycast(ray, out hit, 1000f))
                {
                    if (hit.collider.name == "PaintingWall" && hit.point.y < 10f && hit.point.x > -6.5f && hit.point.x < 6.5f)
                    {
                        GameObject painting = Instantiate(GameManager.instance.Brush, hit.point - Vector3.forward * 0.1f, Quaternion.identity);
                        painting.transform.SetParent(hit.transform);
                        painting.transform.localEulerAngles = new Vector3(90f, 0f, 0f);

                        RenderTexture.active = rt;
                        Texture2D texture2D = new Texture2D(rt.width, rt.height);
                        texture2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
                        texture2D.Apply();

                        Color32[] pixelsInReadTexture = texture2D.GetPixels32();
                        int paintingAmount = 0;
                        foreach (Color color in pixelsInReadTexture)
                        {
                            //Debug.Log(color);
                            if(color == Color.red)
                            {
                                paintingAmount++;
                            }
                        }


                        float percentage = 100f * paintingAmount / 57000;
                        if (percentage > 100f)
                        {
                            percentage = 100f;
                            GameManager.instance.WinGame();
                        }
                        //Debug.Log(percentage + " %");
                        GameManager.instance.TextWallPaintProgress.text = percentage.ToString("F0") + "%";
                        GameManager.instance.SliderWallPaint.value = percentage;
                    }
                }
            }
        }
        else
        {
            rbVelocity = Vector3.zero;
            if (Input.GetMouseButtonUp(0))
            {
                previousMousePosition = Vector3.zero;
                anim.SetTrigger("Idle");
            }
        }

        transform.eulerAngles = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (isUnderForce || GameManager.instance.IsGameEnded) return;

        rb.velocity = rbVelocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Obstacle"))
        {
            ContinueFromNearestCheckPoint();
        }
        else if (collision.collider.CompareTag("RotatingStick"))
        {
            StartCoroutine(ApplyRotatingStickForceEnum(collision.GetContact(0).point));
        }
        else if(collision.collider.CompareTag("RotatingPlatform"))
        {
            transform.SetParent(collision.collider.transform);
        }
        else if (collision.collider.CompareTag("Opponent"))
        {
            Vector3 forceDirection = collision.collider.transform.position - transform.position;
            forceDirection.y = 0;
            forceDirection.Normalize();
            collision.collider.GetComponent<OpponentManager>().ApplyForce();
            rb.AddForce(forceDirection * 2000f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Finish"))
        {
            GameManager.instance.IsGameEnded = true;
            Camera.main.transform.DOMove(new Vector3(0f, 4f, transform.position.z - 8f), 0.5f).SetEase(Ease.Linear);
            Camera.main.transform.DOLocalRotate(Vector3.zero, 0.5f).SetEase(Ease.Linear);
            //Camera.main.transform.DOMove(transform.position, 0.5f).SetEase(Ease.Linear);
            other.GetComponent<BoxCollider>().enabled = false;
            GameManager.instance.SliderWallPaint.gameObject.SetActive(true);
            GameManager.instance.ActivatePaintTheWallText();
            GameManager.instance.RemoveOpponents();
        }
        else if (other.CompareTag("CheckPoint"))
        {
            if (other.transform.GetSiblingIndex() > checkPointIndex)
            {
                checkPointIndex++;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.collider.CompareTag("RotatingPlatform"))
        {
            transform.SetParent(null);
        }
    }

    private IEnumerator ApplyRotatingStickForceEnum(Vector3 halfDonutPosition)
    {
        Vector3 force = transform.position - halfDonutPosition;
        force.y = 0f;
        force.Normalize();
        rb.velocity = Vector3.zero;
        rb.AddForce(force * 4000f);

        isUnderForce = true;
        yield return new WaitForSeconds(1f);
        isUnderForce = false;
    }

    private void ContinueFromNearestCheckPoint()
    {
        if (checkPointIndex == -1)
        {
            transform.position = startPosition;
        }
        else
        {
            Vector3 checkPoint = GameManager.instance.CheckPoints[checkPointIndex].position;
            transform.position = new Vector3(startPosition.x, startPosition.y, checkPoint.z);
        }
    }
}
