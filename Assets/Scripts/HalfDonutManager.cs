﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HalfDonutManager : MonoBehaviour
{
    private float startPosition;
    [SerializeField] private float destination;

    void Start()
    {
        startPosition = transform.GetChild(0).localPosition.x;
        StartCoroutine(MoveEnum());
    }

    private IEnumerator MoveEnum()
    {
        while(true)
        {
            transform.GetChild(0).DOLocalMoveX(destination, 0.5f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            transform.GetChild(0).DOLocalMoveX(startPosition, 0.5f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(0.5f);
            yield return new WaitForSeconds(1f);
        }
    }
}
