﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private Transform pnlLevelCompleted;
    [SerializeField] private Text txtPaintTheWall;

    [SerializeField] private Transform[] opponents;

    [SerializeField] private Transform txtTapToStart;

    public Slider SliderWallPaint;
    public Text TextWallPaintProgress;
    public bool IsGameStarted = false;
    public bool IsGameEnded = false;

    public GameObject Brush;

    public Transform[] CheckPoints;

    [SerializeField] private Transform playerTransform;
    [SerializeField] private int playerRank = -1;
    [SerializeField] private Text txtPlayerRank;

    void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (IsGameEnded) return;

        CalculatePlayerRank();
    }

    public void RemoveOpponents()
    {
        for(int i=0; i < opponents.Length; i++)
        {
            Destroy(opponents[i].gameObject);
        }
    }

    private void CalculatePlayerRank()
    {
        playerRank = opponents.Length + 1;

        for(int i=0; i < opponents.Length; i++)
        {
            if (playerTransform.position.z > opponents[i].position.z) playerRank--;
        }

        txtPlayerRank.text = playerRank.ToString();
    }

    public void StartGame()
    {
        if (IsGameStarted) return;

        for(int i=0; i < opponents.Length; i++)
        {
            opponents[i].GetComponent<Animator>().SetTrigger("Run");
        }

        IsGameStarted = true;
        txtTapToStart.parent.gameObject.SetActive(false);
    }

    public void ActivatePaintTheWallText()
    {
        txtPaintTheWall.gameObject.SetActive(true);
    }

    public void WinGame()
    {
        txtPaintTheWall.gameObject.SetActive(false);
        pnlLevelCompleted.gameObject.SetActive(true);
    }

    public void StartNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
