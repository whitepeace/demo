﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TapToStartTween : MonoBehaviour
{
    private Text txt;

    void Start()
    {
        txt = GetComponent<Text>();
        StartCoroutine(ActivateTweenEnum());
    }


    private IEnumerator ActivateTweenEnum()
    {
        while(true)
        {
            transform.DOScale(Vector3.one * 1.25f, 1f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(1f);
            transform.DOScale(Vector3.one, 1f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(1f);
        }
    }
}
